const form = document.getElementById('form')

if (form) {
  form.addEventListener('submit', (evt) => {
    evt.preventDefault();
    const data = {};
    const resContainer = document.getElementById('results');
    
    data.email = document.getElementById('email').value
    data.name = document.getElementById('name').value
    data.message = document.getElementById('message').value;
  
    fetch('/contact', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(res => {
      if (!res.ok) {
        const errorMessage = 'Sorry, but something went wrong.'
        resContainer.insertAdjacentHTML('afterbegin', `<p><span class="error">${errorMessage}</span></p>`)
      }
      return res.json();
    })
    .then(data => {
      const { name, email, message } = data;
      resContainer.insertAdjacentHTML('afterbegin', `
        <p>Thanks for getting in touch with us. You sent the following information.</p>
        <ul>
          <li>Name: ${name}</li>
          <li>Email: ${email}</li>
          <li>Message: ${message}</li>
        </ul>
        <p>Have a great day!</p>
      `)
    })
    .catch(err => console.error(`Error -> ${err}`))
  })
}
