const express = require('express');
const router = express.Router()

router.get('/', (req, res) => {
  res.send({ message: 'hello API!' })
})

router.get('/users', (req, res) => {
  res.send({ message: 'Users endpoint' })
})

router.get('/users/:name', (req, res) => {
  res.send({
    message: 'Single User Endpoint',
    name: req.params.name
  })
})

module.exports = router;