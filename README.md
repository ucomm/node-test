# README

This is the simplest project I could think of to being testing a node app on OpenShift.

## Usage
Start the project with `docker-compose up`.

### Routes
The project has a few pages at

- /
- /about
- /api-demo
- /contact

These are meant to demonstrate:

- [handlebars](https://handlebarsjs.com) templates/partials
- basic site routing of views with GET
- POSTing a form to the server and getting an AJAX response back
- opening an API endpoint for GET requests

There's also a middleware on the home route which logs to the server.

Because there's currently no persistent storage available, I haven't extended the API to use the other CRUD operations.
