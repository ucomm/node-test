// middleware can be used for things like: validation, security, filtering, etc... before getting to a route.
module.exports = {
  helloWorld: (req, res, next) => {
    console.log('Hello Middleware!')
    // make sure to call next() or the function will hang indefinitely
    next()
  }
}