require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const hbs = require('express-handlebars')
const router = require('./routes/routes');
const { helloWorld } = require('./middleware/middleware');

const app = express();

const port = process.env.PORT || 3000;

// enable template engine
app.engine('hbs', hbs({
  extname: 'hbs', 
  defaultLayout: 'main',
  layoutsDir: `${__dirname}/views/layouts/`,
  partialsDir: `${__dirname}/views/partials/`
 }))
app.set('view engine', 'hbs')

app.use(express.static('public'));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// render views by route
app.get('/', helloWorld, (req, res) => {
  res.render('home', { title: 'OpenShift Test' })
})

app.get('/about', (req, res) => {
  res.render('about', { title: 'OpenShift Test' })
})

app.get('/api-demo', (req, res) => {
  res.render('api-demo', { title: 'OpenShift Test' });
})

app.get('/contact', (req, res) => {
  res.render('contact', { title: 'OpenShift Test' });
})

app.post('/contact', (req, res) => {
  res.send({
    name: req.body.name,
    email: req.body.email,
    message: req.body.message
  })
})

// route API requests
app.use('/api', router)

// listen on the server
app.listen(port, () => console.log(`listening on ${port}`))